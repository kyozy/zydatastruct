// esquirrel3.cpp : Defines the entry point for the DLL application.
//

#include "zydatastruct.h"

#include "elib_sdk/lib2.h"
#include "elib_sdk/lang.h"
#include "elib_sdk/fnshare.h"
#include "elib_sdk/fnshare.cpp"
#include "helper.h"


/************************************************************************/
/* 常量定义
/************************************************************************/
#ifndef __E_STATIC_LIB
//LIB_CONST_INFO s_ConstInfo[] =
//{
	/* { 中文名称, 英文名称, 常量说明, 常量等级(LVL_), 参数类型(CT_), 文本内容, 数值内容 }   只有两种数据类型*/
	// { _WT("测试"), _WT("OT_NULL"), NULL, LVL_SIMPLE, CT_NUM, NULL, OT_NULL },
//};

#endif

// 参数 
ARG_INFO s_arg_hash_create[] =
{
	/* { 参数名称, 参数描述, 图像索引, 图像数量, 参数类型(参见SDT_), 默认数值, 参数类别(参见AS_) } */
	{ _WT("键类型"), _WT("参数值可以为以下常量： 1、#字节型； 2、#短整数型； 3、#整数型； 4、#长整数型； 5、#小数型； 6、#双精度小数型； 7、#逻辑型； 8、#日期时间型； 9、#子程序指针型； 10、#文本型； 11、#字节集型"),0,0, SDT_INT, 0, NULL },
	{ _WT("值类型"), _WT("参数值可以为以下常量： 1、#字节型； 2、#短整数型； 3、#整数型； 4、#长整数型； 5、#小数型； 6、#双精度小数型； 7、#逻辑型； 8、#日期时间型； 9、#子程序指针型； 10、#文本型； 11、#字节集型"),0,0, SDT_INT, 0, NULL },
	{ _WT("初始容量"), _WT("设置合适的初始容量，可以提供效率。默认为：11"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("加载因子"), _WT("当 项目数*加载因子>=容量 则开始扩容。默认为：0.75"),0,0, SDT_FLOAT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("是否有序"), _WT("设置为真表示获取的数据是按照添加的顺序排列的。默认为：假"),0,0, SDT_BOOL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("是否线程安全"), _WT("设置为真表示线程安全。默认为：假"),0,0, SDT_BOOL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_hash_create_from_bytes[] =
{
	/* { 参数名称, 参数描述, 图像索引, 图像数量, 参数类型(参见SDT_), 默认数值, 参数类别(参见AS_) } */
	{ _WT("字节集数据"), _WT("由 对象.保存到字节集（）返回的数据"),0,0, SDT_BIN, 0, NULL },
	{ _WT("初始容量"), _WT("设置合适的初始容量，可以提供效率。默认为：11"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("加载因子"), _WT("当 项目数*加载因子>=容量 则开始扩容。默认为：0.75"),0,0, SDT_FLOAT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("是否有序"), _WT("设置为真表示获取的数据是按照添加的顺序排列的。默认为：假"),0,0, SDT_BOOL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
	{ _WT("是否线程安全"), _WT("设置为真表示线程安全。默认为：假"),0,0, SDT_BOOL, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_hash_put[] =
{
	{ _WT("键"), _WT(""),0,0, _SDT_ALL, 0, NULL },
	{ _WT("值"), _WT(""),0,0, _SDT_ALL, 0, NULL },
};

ARG_INFO s_arg_hash_get[] =
{
	{ _WT("键"), _WT(""),0,0, _SDT_ALL, 0, NULL },
};
ARG_INFO s_arg_hash_contains[] =
{
	{ _WT("值"), _WT(""),0,0, _SDT_ALL, 0, NULL },
};
ARG_INFO s_arg_hash_get_all_keys[] =
{
	{ _WT("键数组"), _WT(""),0,0, _SDT_ALL, 0, AS_RECEIVE_VAR_ARRAY },
};
ARG_INFO s_arg_hash_get_all_values[] =
{
	{ _WT("值数组"), _WT(""),0,0, _SDT_ALL, 0, AS_RECEIVE_VAR_ARRAY },
};

ARG_INFO s_arg_hash_enum_item[] =
{
	{ _WT("回调函数"), _WT("返回值：逻辑型（整数型 用户数据，整数型 键指针，整数型 值指针）"),0,0, _SDT_ALL, 0, 0 },
	{ _WT("用户数据"), _WT("传递给回调函数"),0,0, SDT_INT, 0, AS_DEFAULT_VALUE_IS_EMPTY },
};

ARG_INFO s_arg_hash_set_handle[] =
{
	{ _WT("zyHashMap句柄"), _WT(""),0,0, SDT_INT, 0, 0 },
};
ARG_INFO s_arg_hash_copy[] =
{
	{ _WT("zyHashMap"), _WT(""),0,0, DTP_HASHMAP, 0, 0 },
};
// 命令信息
static CMD_INFO s_CmdInfo[] =
{
	/* { 中文名称, 英文名称, 对象描述, 所属类别(-1是数据类型的方法), 命令状态(CT_), 返回类型(SDT_), 此值保留, 对象等级(LVL_), 图像索引, 图像数量, 参数个数, 参数信息 } */
		//全局命令
	{
		/*ccname*/	_WT("zyAddref"),
		/*egname*/	_WT("zyAddref"),
		/*explain*/	_WT("引用计数加1"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_set_handle) / sizeof(s_arg_hash_set_handle[0]),
		/*arg lp*/	s_arg_hash_set_handle,
	},
	{
		/*ccname*/	_WT("zyRelease"),
		/*egname*/	_WT("zyRelease"),
		/*explain*/	_WT("引用计数减1"),
		/*category*/0,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_set_handle) / sizeof(s_arg_hash_set_handle[0]),
		/*arg lp*/	s_arg_hash_set_handle,
	},
		////////////////////////////////////zyHashMap//////////////////////////////////////
	{
		/*ccname*/	_WT("创建"),
		/*egname*/	_WT("Create"),
		/*explain*/ _WT("任何操作前必须先创建。会自动销毁之前的HashMap"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_create) / sizeof(s_arg_hash_create[0]),
		/*arg lp*/	s_arg_hash_create,
	},
	{
		/*ccname*/	_WT("创建自字节集"),
		/*egname*/	_WT("CreateFromBytes"),
		/*explain*/ _WT("根据 对象.保存到字节集（）的数据创建。会自动销毁之前的HashMap"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_create_from_bytes) / sizeof(s_arg_hash_create_from_bytes[0]),
		/*arg lp*/	s_arg_hash_create_from_bytes,
	},
	{
		/*ccname*/	_WT("取数量"),
		/*egname*/	_WT("GetCount"),
		/*explain*/ _WT("获取项目数"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("添加"),
		/*egname*/	_WT("Put"),
		/*explain*/ _WT("添加一对键值，如果键已经存在，则替换旧值，并返回1；如果新添加则返回0；失败返回-1"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_put) / sizeof(s_arg_hash_put[0]),
		/*arg lp*/	s_arg_hash_put,
	},
	{
		/*ccname*/	_WT("取值"),
		/*egname*/	_WT("Get"),
		/*explain*/ _WT("根据键获取值"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		_SDT_ALL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_get) / sizeof(s_arg_hash_get[0]),
		/*arg lp*/	s_arg_hash_get,
	},
	{
		/*ccname*/	_WT("是否包含值"),
		/*egname*/	_WT("Contains"),
		/*explain*/ _WT("检查指定的值是否存在"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_contains) / sizeof(s_arg_hash_contains[0]),
		/*arg lp*/	s_arg_hash_contains,
	},
	{
		/*ccname*/	_WT("是否包含键"),
		/*egname*/	_WT("ContainsKey"),
		/*explain*/ _WT("检查指定的键是否存在"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_get) / sizeof(s_arg_hash_get[0]),
		/*arg lp*/	s_arg_hash_get,
	},
	{
		/*ccname*/	_WT("移除"),
		/*egname*/	_WT("Remove"),
		/*explain*/ _WT("移除指定键的项目"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BOOL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_get) / sizeof(s_arg_hash_get[0]),
		/*arg lp*/	s_arg_hash_get,
	},
	{
		/*ccname*/	_WT("清除"),
		/*egname*/	_WT("Clear"),
		/*explain*/ _WT("清空所有项目"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,

	},
	{
		/*ccname*/	_WT("取所有键"),
		/*egname*/	_WT("GetAllKeys"),
		/*explain*/ _WT("获取所有键，并保存到参数数组中，返回数组成员数。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_get_all_keys) / sizeof(s_arg_hash_get_all_keys[0]),
		/*arg lp*/	s_arg_hash_get_all_keys,
	},
	{
		/*ccname*/	_WT("取所有值"),
		/*egname*/	_WT("GetAllValuess"),
		/*explain*/ _WT("获取所有值，并保存到参数数组中，返回数组成员数。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_get_all_values) / sizeof(s_arg_hash_get_all_values[0]),
		/*arg lp*/	s_arg_hash_get_all_values,
	},
	{
		/*ccname*/	_WT("保存到字节集"),
		/*egname*/	_WT("SaveToBin"),
		/*explain*/ _WT("将哈希表的所有数据保存到字节集中。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_BIN,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("枚举"),
		/*egname*/	_WT("Enum"),
		/*explain*/ _WT("枚举所有项目。"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_enum_item) / sizeof(s_arg_hash_enum_item[0]),
		/*arg lp*/	s_arg_hash_enum_item,
	},
	{
		/*ccname*/	_WT("置句柄"),
		/*egname*/	_WT("SetHandle"),
		/*explain*/ _WT("引用计数加1，返回当前引用计数"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_set_handle) / sizeof(s_arg_hash_set_handle[0]),
		/*arg lp*/	s_arg_hash_set_handle,
	},
	{
		/*ccname*/	_WT("取句柄"),
		/*egname*/	_WT("GetHandle"),
		/*explain*/ _WT(""),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		SDT_INT,
		/*reserved*/0,
		/*level*/	LVL_HIGH,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("销毁"),
		/*egname*/	_WT("Destory"),
		/*explain*/ _WT("引用计数减一"),
		/*category*/-1,
		/*state*/	0,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("哈希表析构函数"),
		/*egname*/	_WT("HashMapDestruct"),
		/*explain*/	NULL,
		/*category*/-1,
		/*state*/	CT_IS_OBJ_FREE_CMD | CT_IS_HIDED,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},
	{
		/*ccname*/	_WT("哈希表复制函数"),
		/*egname*/	_WT("HashMapCopy"),
		/*explain*/	NULL,
		/*category*/-1,
		/*state*/	CT_IS_OBJ_COPY_CMD | CT_IS_HIDED,
		/*ret*/		_SDT_NULL,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/sizeof(s_arg_hash_copy)/sizeof(s_arg_hash_copy[0]),
		/*arg lp*/	s_arg_hash_copy,
	},
	{
		/*ccname*/	_WT("克隆"),
		/*egname*/	_WT("Clone"),
		/*explain*/	NULL,
		/*category*/-1,
		/*state*/	0,
		/*ret*/		DTP_HASHMAP,
		/*reserved*/0,
		/*level*/	LVL_SIMPLE,
		/*bmp inx*/	0,
		/*bmp num*/	0,
		/*ArgCount*/0,
		/*arg lp*/	NULL,
	},

};

///////////////////////////////////全局函数///////////////////////////////////////
EXTERN_C void zydatastruct_fn_g_Addref(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	zyHashMap* pHashMap = (zyHashMap*)pArgInf[0].m_int;
	if (pHashMap)
	{
		pRetData->m_int = pHashMap->AddRef();
	}
}
EXTERN_C void zydatastruct_fn_g_Release(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	zyHashMap* pHashMap = (zyHashMap*)pArgInf[0].m_int;
	if (pHashMap)
	{
		pRetData->m_int = pHashMap->Release();
	}
}

//////////////////////////////////zyHashMap////////////////////////////////////////
EXTERN_C void zydatastruct_fn_hash_create(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		hHashMap->Release();
	}
	int initialCapacity = 11;
	if (pArgInf[3].m_dtDataType != _SDT_NULL)
	{
		initialCapacity = pArgInf[3].m_int;
	}
	float loadFactor = 0.75;
	if (pArgInf[4].m_dtDataType != _SDT_NULL)
	{
		loadFactor = pArgInf[4].m_float;
	}
	BOOL isOrder = pArgInf[5].m_bool;
	BOOL isThreadSafe = pArgInf[6].m_bool;


	hHashMap = new zyHashMap(pArgInf[1].m_int, pArgInf[2].m_int, isOrder, isThreadSafe, initialCapacity, loadFactor);
	((zyHashMapStruct*)pArgInf[0].m_int)->hHashMap = hHashMap;
	pRetData->m_bool = hHashMap != NULL;
}

EXTERN_C void zydatastruct_fn_hash_create_from_bytes(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		hHashMap->Release();
	}
	if (pArgInf[1].m_pBin == NULL)
	{
		pRetData->m_bool = FALSE;
		return;
	}
	int len = *((LPINT)(pArgInf[1].m_pBin + sizeof(int)));

	int initialCapacity = 11;
	if (pArgInf[2].m_dtDataType != _SDT_NULL)
	{
		initialCapacity = pArgInf[2].m_int;
	}
	float loadFactor = 0.75;
	if (pArgInf[3].m_dtDataType != _SDT_NULL)
	{
		loadFactor = pArgInf[3].m_float;
	}
	BOOL isOrder = pArgInf[4].m_bool;
	BOOL isThreadSafe = pArgInf[5].m_bool;

	hHashMap = zyHashMap::FromBytes (pArgInf[1].m_pBin + sizeof(int)*2, len, isOrder, isThreadSafe, initialCapacity, loadFactor);
	((zyHashMapStruct*)pArgInf[0].m_int)->hHashMap = hHashMap;
	pRetData->m_bool = hHashMap != NULL;
}

EXTERN_C void zydatastruct_fn_hash_get_count(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		pRetData->m_int = hHashMap->GetCount();
	}
	
}


EXTERN_C void zydatastruct_fn_hash_put(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	pRetData->m_int = -1;
	if (hHashMap)
	{
		pRetData->m_int = hHashMap->Put(&pArgInf[1], &pArgInf[2]);
	}
	
}

EXTERN_C void zydatastruct_fn_hash_get(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		hHashMap->Get(pRetData, &pArgInf[1]);
	}
}
EXTERN_C void zydatastruct_fn_hash_contains(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		pRetData->m_bool = hHashMap->Contains(&pArgInf[1]);
	}
}
EXTERN_C void zydatastruct_fn_hash_contains_key(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		pRetData->m_bool = hHashMap->ContainsKey(&pArgInf[1]);
	}
}
EXTERN_C void zydatastruct_fn_hash_remove(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		pRetData->m_bool = hHashMap->Remove(&pArgInf[1]);
	}
}
EXTERN_C void zydatastruct_fn_hash_clear(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		hHashMap->Clear();
	}
}
EXTERN_C void zydatastruct_fn_hash_get_all_keys(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		pRetData->m_int = hHashMap->GetAllKeys(&pArgInf[1]);
	}
}
EXTERN_C void zydatastruct_fn_hash_get_all_values(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		pRetData->m_int = hHashMap->GetAllValues(&pArgInf[1]);
	}
}

EXTERN_C void zydatastruct_fn_hash_save_to_bin(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		pRetData->m_pBin = hHashMap->SaveToBin();
	}
}
EXTERN_C void zydatastruct_fn_hash_enum_item(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		hHashMap->EnumItem((zyHashEnumCallback)pArgInf[1].m_dwSubCodeAdr, (void*)pArgInf[2].m_int);
	}
}

EXTERN_C void zydatastruct_fn_hash_set_handle(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	zyHashMap* pHashMap = (zyHashMap*)pArgInf[1].m_int;
	int refCount = -1;
	if (pHashMap)
	{
		refCount = pHashMap->AddRef();
	}
	if (hHashMap)
	{
		if (hHashMap == pHashMap)
		{
			refCount = hHashMap->Release();
		}
		else
		{
			hHashMap->Release();
		}
	}
	((zyHashMapStruct*)pArgInf[0].m_pCompoundData)->hHashMap = pHashMap;
	pRetData->m_int = refCount;
	
}
EXTERN_C void zydatastruct_fn_hash_get_handle(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	pRetData->m_int = (int)hHashMap;
}
EXTERN_C void zydatastruct_fn_hash_destory(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		hHashMap->Release();
		((zyHashMapStruct*)pArgInf[0].m_pCompoundData)->hHashMap = NULL;
	}
	
}
//析构函数
EXTERN_C void zydatastruct_fn_hash_destruct(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	if (hHashMap)
	{
		hHashMap->Release();
	}
}
//复制构造函数
EXTERN_C void zydatastruct_fn_hash_copy(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	zyHashMap* pHashMap = ((zyHashMapStruct*)pArgInf[1].m_pCompoundData)->hHashMap;
	if (pHashMap)
	{
		pHashMap->AddRef();
	}
	((zyHashMapStruct*)pArgInf[0].m_pCompoundData)->hHashMap = pHashMap;
}

EXTERN_C void zydatastruct_fn_hash_clone(PMDATA_INF pRetData, INT iArgCount, PMDATA_INF pArgInf)
{
	SETUP_HASHMAP(pArgInf);
	CFreqMem fm;
	if (hHashMap)
	{
		fm.AddDWord((DWORD)hHashMap->Clone());
	}
	else
	{
		fm.AddDWord(0);
	}
	pRetData->m_pCompoundData = fm.GetPtr();
	
}



#ifndef __E_STATIC_LIB

PFN_EXECUTE_CMD s_RunFunc[] =	// 索引应与s_CmdInfo中的命令定义顺序对应
{
	zydatastruct_fn_g_Addref,
	zydatastruct_fn_g_Release,
	//哈希表
	zydatastruct_fn_hash_create,
	zydatastruct_fn_hash_create_from_bytes,
	zydatastruct_fn_hash_get_count,
	zydatastruct_fn_hash_put,
	zydatastruct_fn_hash_get,
	zydatastruct_fn_hash_contains,
	zydatastruct_fn_hash_contains_key,
	zydatastruct_fn_hash_remove,
	zydatastruct_fn_hash_clear,
	zydatastruct_fn_hash_get_all_keys,
	zydatastruct_fn_hash_get_all_values,
	zydatastruct_fn_hash_save_to_bin,
	zydatastruct_fn_hash_enum_item,
	zydatastruct_fn_hash_set_handle,
	zydatastruct_fn_hash_get_handle,
	zydatastruct_fn_hash_destory,
	zydatastruct_fn_hash_destruct,
	zydatastruct_fn_hash_copy,
	zydatastruct_fn_hash_clone,

	
};

static const char* const g_CmdNames[] =
{
	"zydatastruct_fn_g_Addref",
	"zydatastruct_fn_g_Release",
	"zydatastruct_fn_hash_create",
	"zydatastruct_fn_hash_create_from_bytes",
	"zydatastruct_fn_hash_get_count",
	"zydatastruct_fn_hash_put",
	"zydatastruct_fn_hash_get",
	"zydatastruct_fn_hash_contains",
	"zydatastruct_fn_hash_contains_key",
	"zydatastruct_fn_hash_remove",
	"zydatastruct_fn_hash_clear",
	"zydatastruct_fn_hash_get_all_keys",
	"zydatastruct_fn_hash_get_all_values",
	"zydatastruct_fn_hash_save_to_bin",
	"zydatastruct_fn_hash_enum_item",
	"zydatastruct_fn_hash_set_handle",
	"zydatastruct_fn_hash_get_handle",
	"zydatastruct_fn_hash_destory",
	"zydatastruct_fn_hash_destruct",
	"zydatastruct_fn_hash_copy",
	"zydatastruct_fn_hash_clone",
};

#endif


/************************************************************************/
/* 数据类型定义
/************************************************************************/

#ifndef __E_STATIC_LIB


LIB_DATA_TYPE_ELEMENT s_dt_element_hashmap[] =
{
	/*{ 成员类型 ,数组成员 , 中文名称 ,英文名称 ,成员解释 ,成员状态 ,默认值}*/
	{ SDT_INT, NULL,_WT("hHashMap"), _WT("hHashMap"), _WT("hHashMap"), LES_HIDED, 0 },

};

INT s_hashCommandIndexs[] =
{
	2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
};

static LIB_DATA_TYPE_INFO s_DataTypes[] =
{
	/* { 中文名称, 英文名称, 数据描述, 索引数量, 命令索引, 对象状态, 图标索引, 事件数量, 事件指针, 属性数量, 属性指针, 界面指针, 元素数量, 元素指针 } */
	{ _WT("zyHashMap"), _WT("zyHashMap"), _WT("一个键值对容器"), sizeof(s_hashCommandIndexs) / sizeof(s_hashCommandIndexs[0]), s_hashCommandIndexs, NULL, 0, 0, NULL, 0, NULL, NULL, sizeof(s_dt_element_hashmap) / sizeof(s_dt_element_hashmap[0]), s_dt_element_hashmap },
};



#endif

EXTERN_C INT WINAPI  zydatastruct_ProcessNotifyLib(INT nMsg, DWORD dwParam1, DWORD dwParam2)
{
#ifndef __E_STATIC_LIB
	if (nMsg == NL_GET_CMD_FUNC_NAMES) // 返回所有命令实现函数的的函数名称数组(char*[]), 支持静态编译的动态库必须处理
		return (INT)g_CmdNames;
	else if (nMsg == NL_GET_NOTIFY_LIB_FUNC_NAME) // 返回处理系统通知的函数名称(PFN_NOTIFY_LIB函数名称), 支持静态编译的动态库必须处理
		return (INT)"zydatastruct_ProcessNotifyLib";
	else if (nMsg == NL_GET_DEPENDENT_LIBS) return NULL;
	// 返回静态库所依赖的其它静态库文件名列表(格式为\0分隔的文本,结尾两个\0), 支持静态编译的动态库必须处理
	// kernel32.lib user32.lib gdi32.lib 等常用的系统库不需要放在此列表中
	// 返回NULL或NR_ERR表示不指定依赖文件  
#endif
	return ProcessNotifyLib(nMsg, dwParam1, dwParam2);
};

#ifndef __E_STATIC_LIB
static LIB_INFO s_LibInfo =
{
		LIB_FORMAT_VER,				//库格式号
		_T(LIB_GUID_STR),			//GUID
		LIB_MajorVersion,			//本库的主版本号
		LIB_MinorVersion,			//本库的次版本号
		LIB_BuildNumber,			//构建版本号
		LIB_SysMajorVer,			//所需要的易语言系统的主版本号
		LIB_SysMinorVer,			//所需要的易语言系统的次版本号
		LIB_KrnlLibMajorVer,		//所需要的系统核心支持库的主版本号
		LIB_KrnlLibMinorVer,		//所需要的系统核心支持库的次版本号
		_T(LIB_NAME_STR),			//库名
		__GBK_LANG_VER,				//库所支持的语言
		_WT(LIB_DESCRIPTION_STR),	//库详细解释
		
#ifndef __COMPILE_FNR			//状态
		/*dwState*/				_LIB_OS(__OS_WIN),
#else
		/*dwState*/				LBS_NO_EDIT_INFO | _LIB_OS(__OS_WIN) | LBS_LIB_INFO2,
#endif	
		
		_WT(LIB_Author),			//作者
		_WT(LIB_ZipCode),			//邮政编码
		NULL,				//通信地址
		NULL,				//电话号码
		NULL,				//传真
		NULL,				//邮箱地址
		NULL,				//主页
		NULL,				//其他信息
		
		//自定义数据类型
		sizeof(s_DataTypes) / sizeof(s_DataTypes[0]),
		s_DataTypes,
		//类别说明
#ifndef __COMPILE_FNR
		/*CategoryCount*/   LIB_TYPE_COUNT,			// 加了类别需加此值。
		/*category*/       	_WT(LIB_TYPE_STR),			// 类别说明表每项为一字符串,前四位数字表示图象索引号(从1开始,0无).
		/*CmdCount*/        sizeof(s_CmdInfo) / sizeof(s_CmdInfo[0]),
		/*BeginCmd*/        s_CmdInfo,
#else
		// fnr版本不需要以下信息
		/*CategoryCount*/   0,
		/*category*/        NULL,
		/*CmdCount*/        0,
		/*BeginCmd*/        NULL,
#endif
		/*m_pCmdsFunc*/             s_RunFunc,
		/*pfnRunAddInFn*/			NULL,
		/*szzAddInFnInfo*/			NULL,
		
		/*pfnNotify*/				zydatastruct_ProcessNotifyLib,
		
		/*pfnRunSuperTemplate*/		NULL,
		/*szzSuperTemplateInfo*/	NULL,
		
#ifndef __COMPILE_FNR
		/*nLibConstCount*/			0,//sizeof(s_ConstInfo)/ sizeof(s_ConstInfo[0]),
		/*pLibConst*/				0,//s_ConstInfo,
#else
		// fnr版本不需要以下信息
		/*nLibConstCount*/			0,
		/*pLibConst*/				NULL,
#endif
		
		/*szzDependFiles*/			NULL
		
};

PLIB_INFO WINAPI GetNewInf()
{
	return (&s_LibInfo);
};

#endif