
#include "zyMutex.h"


HANDLE zyMutexInit()
{
	return CreateMutex(NULL, FALSE, NULL);
}

void zyMutexDestory(HANDLE hMutex)
{
	if (hMutex)
	{
		CloseHandle(hMutex);
	}
	
}

void zyMutexLock(HANDLE hMutex)
{
	if (hMutex)
	{
		WaitForSingleObject(hMutex, INFINITE);
	}
}

BOOL zyMutexUnLock(HANDLE hMutex)
{
	if (hMutex)
	{
		return ReleaseMutex(hMutex);
	}
	return FALSE;
}

