#include "zyHashMap.h"
#include "zyMutex.h"
#include "helper.h"
#include "elib_sdk/fnshare.h"
#include <sstream>

zyHashMap::zyHashMap(DWORD keyType, DWORD valType, BOOL isOrder /*= false*/, BOOL threadSafe /*= false*/, int initialCapacity /*= 11*/, float loadFactor /*= 0.75*/) 
	:m_isOrder(isOrder), m_tableLenth(initialCapacity), m_loadFactor(loadFactor), m_mutex(NULL), m_orderHead(NULL)
{

	m_count = 0;
	m_refCount = 1;
	m_table = (zyEntry**)malloc(m_tableLenth * sizeof(zyEntry*));
	memset(m_table, 0, m_tableLenth * sizeof(zyEntry*));
	m_threshold = (int)(m_tableLenth * loadFactor);
	m_keyType = zyHashMap::GetLibType(keyType);
	m_valType = zyHashMap::GetLibType(valType);

	if (threadSafe)
	{
		m_mutex = zyMutexInit();
	}
}


zyHashMap* zyHashMap::FromBytes(LPBYTE bytes, int length, BOOL isOrder /*= false*/, BOOL threadSafe /*= false*/, int initialCapacity /*= 11*/, float loadFactor /*= 0.75*/)
{
	if (!bytes || length < 16)
	{
		return NULL;
	}
	if (bytes[0]!='h' || bytes[1] != 'a' || bytes[2] != 's' || bytes[3] != 'h')
	{
		return NULL;
	}
	int eKeyType = *((DWORD*)(bytes + 4));
	DWORD keyType = zyHashMap::GetLibType(eKeyType);

	int eValType = *((DWORD*)(bytes + 8));
	DWORD valType = zyHashMap::GetLibType(eValType);

	int count = *((LPINT)(bytes + 12));

	if (keyType == _SDT_NULL || valType == _SDT_NULL)
	{
		return NULL;
	}

	int keyLen = zyHashMap::GetTypeSize(keyType);
	int valLen = zyHashMap::GetTypeSize(valType);
	if (16 + count * (keyLen + valLen) > length)
	{
		return NULL;
	}

	zyHashMap* hHashMap = new zyHashMap(eKeyType, eValType, isOrder, threadSafe, initialCapacity, loadFactor);
	if (!hHashMap)
	{
		return NULL;
	}

	MDATA_INF key, val;
	key.m_dtDataType = keyType;
	val.m_dtDataType = valType;

	LPBYTE offsetTable = bytes + 16;


	int offset = 0;

	for (int i = 0; i < count; ++i)
	{
		switch (keyType)
		{
		case SDT_BYTE:
			key.m_byte = *(offsetTable + i * (keyLen + valLen));
			break;
		case SDT_SHORT:
			key.m_short = *((short*)(offsetTable + i * (keyLen + valLen)));
			break;
		case SDT_INT:
		case SDT_BOOL:
		case SDT_SUB_PTR:
			key.m_int = *((int*)(offsetTable + i * (keyLen + valLen)));
			break;
		case SDT_INT64:
			key.m_int64 = *((__int64*)(offsetTable + i * (keyLen + valLen)));
			break;
		case SDT_FLOAT:
			key.m_float = *((float*)(offsetTable + i * (keyLen + valLen)));
			break;
		case SDT_DOUBLE:
			key.m_double = *((double*)(offsetTable + i * (keyLen + valLen)));
			break;
		case SDT_TEXT:
		{
			offset = *((int*)(offsetTable + i * (keyLen + valLen)));
			if (offset == 0)
			{
				key.m_pText = NULL;
			}
			else
			{
				key.m_pText = (char*)(bytes + offset);
			}
			break;
		}
		
		case SDT_BIN:
			offset = *((int*)(offsetTable + i * (keyLen + valLen)));
			if (offset == 0)
			{
				key.m_pBin = NULL;
			}
			else
			{
				key.m_pBin = (LPBYTE)(bytes + offset);
			}
			break;
		default:
			break;
		}


		switch (valType)
		{
		case SDT_BYTE:
			val.m_byte = *(offsetTable + i * (keyLen + valLen) + keyLen);
			break;
		case SDT_SHORT:
			val.m_short = *((short*)(offsetTable + i * (keyLen + valLen) + keyLen));
			break;
		case SDT_INT:
		case SDT_BOOL:
		case SDT_SUB_PTR:
			val.m_int = *((int*)(offsetTable + i * (keyLen + valLen) + keyLen));
			break;
		case SDT_INT64:
			val.m_int64 = *((__int64*)(offsetTable + i * (keyLen + valLen) + keyLen));
			break;
		case SDT_FLOAT:
			val.m_float = *((float*)(offsetTable + i * (keyLen + valLen) + keyLen));
			break;
		case SDT_DOUBLE:
			val.m_double = *((double*)(offsetTable + i * (keyLen + valLen) + keyLen));
			break;
		case SDT_TEXT:
			offset = *((int*)(offsetTable + i * (keyLen + valLen) + keyLen));
			if (offset == 0)
			{
				val.m_pText = NULL;
			}
			else
			{
				val.m_pText = (char*)(bytes + offset);
			}
			
			break;
		case SDT_BIN:
			offset = *((int*)(offsetTable + i * (keyLen + valLen) + keyLen));
			if (offset == 0)
			{
				val.m_pBin = NULL;
			}
			else
			{
				val.m_pBin = (LPBYTE)(bytes + offset);
			}
			break;
		default:
			break;
		}

		if (hHashMap->Put(&key, &val) == -1)
		{
			hHashMap->Release();
			return NULL;
		}
	}

	return hHashMap;

}

zyHashMap::~zyHashMap()
{
	zyMutexLock(m_mutex);
	for (int i = 0; i < m_tableLenth; ++i)
	{
		zyEntry* pe = m_table[i];
		while (pe)
		{
			zyEntry* oldPe = pe->next;
			if (m_keyType == SDT_TEXT || m_keyType == SDT_BIN)
			{
				if (pe->key.value)
				{
					free(pe->key.value);
				}

			}
			if (m_valType == SDT_TEXT || m_valType == SDT_BIN)
			{
				if (pe->value.value)
				{
					free(pe->value.value);
				}

			}
			free(pe);
			pe = oldPe;
		}
	}
	free(m_table);

	zyMutexUnLock(m_mutex);
	zyMutexDestory(m_mutex);

}

unsigned long __stdcall zyHashMap::QueryInterface(const char* iid, IZYKnown **ppv)
{
	*ppv = NULL;
	return 0;
}

unsigned long __stdcall zyHashMap::AddRef()
{
	InterlockedIncrement(&m_refCount);
	return m_refCount;

}

unsigned long __stdcall zyHashMap::Release()
{
	InterlockedDecrement(&m_refCount);
	if (m_refCount == 0)
	{
		delete this;
		return 0;
	}
	return m_refCount;
}

int zyHashMap::Put(PMDATA_INF pArgKey, PMDATA_INF pArgVal)
{
	size_t hashCode = zyHashMap::GetHashCode(pArgKey); // 获取哈希值
	zyMutexLock(m_mutex);

	size_t index = (hashCode & 0x7FFFFFFF) % m_tableLenth; // 计算出索引
	zyEntry* pe = m_table[index];
	while (pe)
	{
		if (hashCode == pe->hashCode && zyHashMap::Equal(m_keyType, pArgKey, pe->key)) //如果存在相同键，替换旧值
		{
			if (m_valType == SDT_TEXT || m_valType == SDT_BIN)
			{
				if (pe->value.value)
				{
					free(pe->value.value);
				}
			}
			if (!zyHashMap::E2Hash(m_valType, pArgVal, pe->value))
			{
				zyMutexUnLock(m_mutex);
				return -1;
			}
			zyMutexUnLock(m_mutex);
			return 1;
		}
		pe = pe->next;
	}

	if (m_count >= m_threshold) // 需要重新分配内存
	{
		if (!this->ReHash())
		{
			zyMutexUnLock(m_mutex);
			return -1;
		}
		index = (hashCode & 0x7FFFFFFF) % m_tableLenth; //重新计算索引
	}
	
	pe = (zyEntry*)malloc(sizeof(zyEntry));
	if (!pe)
	{
		zyMutexUnLock(m_mutex);
		return -1;
	}
	pe->hashCode = hashCode;
	zyHashMap::E2Hash(m_keyType, pArgKey, pe->key);
	zyHashMap::E2Hash(m_valType, pArgVal, pe->value);
	pe->next = m_table[index];
	m_table[index] = pe;
	pe->orderNext = NULL;
	pe->orderPrev = NULL;
	if (m_isOrder)
	{
		if (!m_orderHead)
		{
			pe->orderPrev = pe;
			m_orderHead = pe;
		}
		else
		{
			zyEntry* last = m_orderHead->orderPrev;
			last->orderNext = pe;
			pe->orderPrev = last;
			m_orderHead->orderPrev = pe;
		}
	}

	m_count++;
	zyMutexUnLock(m_mutex);
	return 0;
}


void zyHashMap::Get(PMDATA_INF pRet, PMDATA_INF pArgKey)
{

	size_t hashCode = zyHashMap::GetHashCode(pArgKey); // 获取哈希值
	zyMutexLock(m_mutex);

	size_t index = (hashCode & 0x7FFFFFFF) % m_tableLenth; // 计算出索引
	zyEntry* pe = m_table[index];
	while (pe)
	{
		if (hashCode == pe->hashCode && zyHashMap::Equal(m_keyType, pArgKey, pe->key))
		{
			zyHashMap::Hash2E(m_valType, pe->value, pRet);
			zyMutexUnLock(m_mutex);
			return;
		}
		pe = pe->next;
	}
	zyMutexUnLock(m_mutex);
	pRet->m_dtDataType = m_valType;
	pRet->m_int64 = 0;
}

BOOL zyHashMap::Contains(PMDATA_INF pArgVal)
{
	zyMutexLock(m_mutex);
	for (int i = 0; i < m_tableLenth; i++)
	{
		zyEntry *pe = m_table[i];
		while (pe)
		{
			if (zyHashMap::Equal(m_valType, pArgVal, pe->value))
			{
				zyMutexUnLock(m_mutex);
				return TRUE;
			}
			pe = pe->next;
		}
	}
	zyMutexUnLock(m_mutex);
	return FALSE;
}

BOOL zyHashMap::ContainsKey(PMDATA_INF pArgKey)
{
	size_t hashCode = zyHashMap::GetHashCode(pArgKey); // 获取哈希值
	zyMutexLock(m_mutex);

	size_t index = (hashCode & 0x7FFFFFFF) % m_tableLenth; // 计算出索引
	zyEntry* pe = m_table[index];
	while (pe)
	{
		if (hashCode == pe->hashCode && zyHashMap::Equal(m_keyType, pArgKey, pe->key))
		{
			zyMutexUnLock(m_mutex);
			return TRUE;
		}
		pe = pe->next;
	}
	zyMutexUnLock(m_mutex);
	return FALSE;
}

BOOL zyHashMap::Remove(PMDATA_INF pArgKey)
{
	size_t hashCode = zyHashMap::GetHashCode(pArgKey); // 获取哈希值
	zyMutexLock(m_mutex);

	size_t index = (hashCode & 0x7FFFFFFF) % m_tableLenth; // 计算出索引
	zyEntry *pe = m_table[index];
	zyEntry *prev = NULL;
	while (pe)
	{
		if (hashCode == pe->hashCode && zyHashMap::Equal(m_keyType, pArgKey, pe->key))
		{
			if (prev)
			{
				prev->next = pe->next;
			}
			else 
			{ 
				m_table[index] = pe->next; 
			}

			//释放内存
			if (m_keyType == SDT_TEXT || m_keyType == SDT_BIN)
			{
				if (pe->key.value)
				{
					free(pe->key.value);
				}

			}
			if (m_valType == SDT_TEXT || m_valType == SDT_BIN)
			{
				if (pe->value.value)
				{
					free(pe->value.value);
				}

			}
			//从有序链表中移除
			if (m_isOrder)
			{
				zyEntry* link_prev = pe->orderPrev;
				zyEntry* link_next = pe->orderNext;
				if (m_orderHead == pe) //是头部
				{
					m_orderHead = link_next;
				}
				else 
				{
					link_prev->orderNext = link_next;
					if (link_next == NULL) //是尾部
					{
						m_orderHead->orderPrev = link_prev;
					}
					if (link_next)
					{
						link_next->orderPrev = link_prev;
					}
				}
			}

			free(pe);
			m_count--;

			zyMutexUnLock(m_mutex);
			return TRUE;
		}
		prev = pe;
		pe = pe->next;
	}
	zyMutexUnLock(m_mutex);
	return FALSE;
}

void zyHashMap::Clear()
{
	zyMutexLock(m_mutex);
	for (int i = 0; i < m_tableLenth; i++)
	{
		zyEntry * pe = m_table[i];
		while (pe)
		{
			zyEntry * next = pe->next;
			//释放内存
			if (m_keyType == SDT_TEXT || m_keyType == SDT_BIN)
			{
				if (pe->key.value)
				{
					free(pe->key.value);
				}

			}
			if (m_valType == SDT_TEXT || m_valType == SDT_BIN)
			{
				if (pe->value.value)
				{
					free(pe->value.value);
				}

			}
			free(pe);
			pe = next;
		}
	}
	m_count = 0;
	memset(m_table, 0, m_tableLenth * sizeof(zyEntry*));
	zyMutexUnLock(m_mutex);
}

int zyHashMap::GetCount() const
{
	return m_count;
}

int zyHashMap::GetAllKeys(PMDATA_INF pArgKeys)
{
	if (pArgKeys->m_dtDataType != m_keyType)
	{
		return 0;
	}
	zyMutexLock(m_mutex);

	NotifySys(NRS_FREE_ARY, (DWORD)pArgKeys->m_dtDataType, (DWORD)*pArgKeys->m_ppAryData);
	INT nSize = 0;
	switch (m_keyType)
	{
	case SDT_BYTE:
		nSize = m_count * sizeof(BYTE);
		break;
	case SDT_SHORT:
		nSize = m_count * sizeof(short);
		break;
	case SDT_INT:
	case SDT_SUB_PTR:
	case SDT_BOOL:
	case SDT_TEXT:
	case SDT_BIN:
	case SDT_FLOAT:
		nSize = m_count * sizeof(DWORD);
		break;
	case SDT_INT64:
	case SDT_DOUBLE:
	case SDT_DATE_TIME:
		nSize = m_count * sizeof(double);
		break;
	default:
		break;
	}
	LPBYTE p = (LPBYTE)MMalloc(sizeof(INT) * 2 + nSize);
	*(LPINT)p = 1;  // 数组维数。
	*(LPINT)(p + sizeof(INT)) = m_count;
	LPBYTE ptrData = p + sizeof(INT) * 2;
	zyEntry * pe = NULL;
	if (m_isOrder)
	{
		pe = m_orderHead;
		while (pe)
		{
			ptrData = zyHashMap::SetData(m_keyType, ptrData, pe->key);
			pe = pe->orderNext;
		}
	}
	else
	{
		for (int i = 0; i < m_tableLenth; i++)
		{
			pe = m_table[i];
			while (pe)
			{
				ptrData = zyHashMap::SetData(m_keyType, ptrData, pe->key);
				pe = pe->next;
			}
		}
	}
	*pArgKeys->m_ppAryData = p;  // 将新内容写回该数组变量。
	zyMutexUnLock(m_mutex);
	return m_count;
}

int zyHashMap::GetAllValues(PMDATA_INF pArgValues)
{
	if (pArgValues->m_dtDataType != m_valType)
	{
		return 0;
	}
	zyMutexLock(m_mutex);

	NotifySys(NRS_FREE_ARY, (DWORD)pArgValues->m_dtDataType, (DWORD)*pArgValues->m_ppAryData);

	INT nSize = 0;
	switch (m_valType)
	{
	case SDT_BYTE:
		nSize = m_count * sizeof(BYTE);
		break;
	case SDT_SHORT:
		nSize = m_count * sizeof(short);
		break;
	case SDT_INT:
	case SDT_SUB_PTR:
	case SDT_BOOL:
	case SDT_TEXT:
	case SDT_BIN:
	case SDT_FLOAT:
		nSize = m_count * sizeof(DWORD);
		break;
	case SDT_INT64:
	case SDT_DOUBLE:
	case SDT_DATE_TIME:
		nSize = m_count * sizeof(double);
		break;
	default:
		break;
	}
	LPBYTE p = (LPBYTE)MMalloc(sizeof(INT) * 2 + nSize);
	*(LPINT)p = 1;  // 数组维数。
	*(LPINT)(p + sizeof(INT)) = m_count;
	LPBYTE ptrData = p + sizeof(INT) * 2;
	zyEntry * pe = NULL;

	if (m_isOrder)
	{
		pe = m_orderHead;
		while (pe)
		{
			ptrData = zyHashMap::SetData(m_valType, ptrData, pe->value);
			pe = pe->orderNext;
		}
	}
	else
	{
		for (int i = 0; i < m_tableLenth; i++)
		{
			pe = m_table[i];
			while (pe)
			{
				ptrData = zyHashMap::SetData(m_valType, ptrData, pe->value);
				pe = pe->next;
			}
		}
	}

	*pArgValues->m_ppAryData = p;  // 将新内容写回该数组变量。
	zyMutexUnLock(m_mutex);
	return m_count;
}

int zyHashMap::GetKeysFromValue(PMDATA_INF pArgValue, PMDATA_INF pArgKeys)
{
	if (pArgKeys->m_dtDataType != m_keyType)
	{
		return 0;
	}
	zyMutexLock(m_mutex);

	NotifySys(NRS_FREE_ARY, (DWORD)pArgKeys->m_dtDataType, (DWORD)*pArgKeys->m_ppAryData);
	INT nSize = 0;
	switch (m_keyType)
	{
	case SDT_BYTE:
		nSize = m_count * sizeof(BYTE);
		break;
	case SDT_SHORT:
		nSize = m_count * sizeof(short);
		break;
	case SDT_INT:
	case SDT_SUB_PTR:
	case SDT_BOOL:
	case SDT_TEXT:
	case SDT_BIN:
	case SDT_FLOAT:
		nSize = m_count * sizeof(DWORD);
		break;
	case SDT_INT64:
	case SDT_DOUBLE:
	case SDT_DATE_TIME:
		nSize = m_count * sizeof(double);
		break;
	default:
		break;
	}
	LPBYTE p = (LPBYTE)MMalloc(sizeof(INT) * 2 + nSize);
	*(LPINT)p = 1;  // 数组维数。
	int count = 0;
	//*(LPINT)(p + sizeof(INT)) = m_count;
	LPBYTE ptrData = p + sizeof(INT) * 2;

	for (int i = 0; i < m_tableLenth; i++)
	{
		zyEntry * pe = m_table[i];
		while (pe)
		{
			if (zyHashMap::Equal(m_valType, pArgValue, pe->value))
			{
				
				count++;
			}
			
			pe = pe->next;
		}
	}
	*(LPINT)(p + sizeof(INT)) = count;
	*pArgKeys->m_ppAryData = p;  // 将新内容写回该数组变量。
	zyMutexUnLock(m_mutex);
	return count;
}

zyHashMap * zyHashMap::Clone()
{
	zyMutexLock(m_mutex);
	zyHashMap* pHashMap = new zyHashMap(zyHashMap::GetEType(m_keyType), zyHashMap::GetEType(m_valType), m_isOrder, m_mutex != NULL, m_tableLenth, m_loadFactor);
	if (!pHashMap)
	{
		zyMutexUnLock(m_mutex);
		return NULL;
	}
	
	for (int i = 0; i < m_tableLenth; i++)
	{
		zyEntry* pe = m_table[i];
		while (pe)
		{
			bool b = false;
			size_t index = (pe->hashCode & 0x7FFFFFFF) % pHashMap->m_tableLenth; // 计算出索引;
			if (pHashMap->m_count >= pHashMap->m_threshold) // 需要重新分配内存
			{
				b = pHashMap->ReHash();
				if (!b)
				{
					zyMutexUnLock(m_mutex);
					pHashMap->Release();
					return NULL;
				}
				index = (pe->hashCode & 0x7FFFFFFF) % pHashMap->m_tableLenth; //重新计算索引
			}
			zyEntry* newPe = new zyEntry();
			if (!newPe)
			{
				zyMutexUnLock(m_mutex);
				pHashMap->Release();
				return NULL;
			}
			newPe->hashCode = pe->hashCode;
			zyHashMap::Hash2Hash(m_keyType, pe->key, newPe->key);
			zyHashMap::Hash2Hash(m_valType, pe->value, newPe->value);
			newPe->next = pHashMap->m_table[index];
			pHashMap->m_table[index] = newPe;
			newPe->orderNext = NULL;
			newPe->orderPrev = NULL;
			if (m_isOrder)
			{
				if (!pHashMap->m_orderHead)
				{
					newPe->orderPrev = newPe;
					pHashMap->m_orderHead = newPe;
				}
				else
				{
					zyEntry* last = pHashMap->m_orderHead->orderPrev;
					last->orderNext = newPe;
					newPe->orderPrev = last;
					pHashMap->m_orderHead->orderPrev = newPe;
				}
			}

			pHashMap->m_count++;
			pe = pe->next;
		}
	}
	zyMutexUnLock(m_mutex);
	return pHashMap;
}

//const zyHashMap* zyHashMap::Copy(const zyHashMap* rHash)
//{
//	if (this == rHash || rHash == NULL) // 指针相等
//	{
//		return NULL;
//	}
//	this->Clear();
//	zyMutexLock(m_mutex);
//	delete m_table;
//	m_table = new zyEntry*[rHash->m_tableLenth];
//	memset(m_table, 0, m_tableLenth * sizeof(void*));
//
//	m_tableLenth = rHash->m_tableLenth;
//	m_count = 0;
//	m_threshold = (size_t)(rHash->m_threshold * rHash->m_loadFactor);
//	m_loadFactor = rHash->m_loadFactor;
//	m_isOrder = rHash->m_isOrder;
//	m_keyType = rHash->m_keyType;
//	m_valType = rHash->m_valType;
//
//	for (int i = 0; i < rHash->m_tableLenth; i++)
//	{
//		zyEntry* pe = rHash->m_table[i];
//		while (pe)
//		{
//			bool b = false;
//			size_t index = (pe->hashCode & 0x7FFFFFFF) % m_tableLenth; // 计算出索引;
//			if (m_count >= m_threshold) // 需要重新分配内存
//			{
//				b = this->ReHash();
//				if (!b)
//				{
//					zyMutexUnLock(m_mutex);
//					return NULL;
//				}
//				index = (pe->hashCode & 0x7FFFFFFF) % m_tableLenth; //重新计算索引
//			}
//			zyEntry* newPe = new zyEntry();
//			if (!newPe)
//			{
//				zyMutexUnLock(m_mutex);
//				return NULL;
//			}
//			newPe->hashCode = pe->hashCode;
//			zyHashMap::Hash2Hash(m_keyType,  pe->key, newPe->key);
//			zyHashMap::Hash2Hash(m_valType, pe->value, newPe->value);
//			newPe->next = m_table[index];
//			m_table[index] = newPe;
//			newPe->orderNext = NULL;
//			newPe->orderPrev = NULL;
//			if (m_isOrder)
//			{
//				if (!m_orderHead)
//				{
//					newPe->orderPrev = newPe;
//					m_orderHead = newPe;
//				}
//				else
//				{
//					zyEntry* last = m_orderHead->orderPrev;
//					last->orderNext = newPe;
//					newPe->orderPrev = last;
//					m_orderHead->orderPrev = newPe;
//				}
//			}
//
//			m_count++;
//			pe = pe->next;
//		}
//	}
//	zyMutexUnLock(m_mutex);
//	return this;
//}

LPBYTE zyHashMap::SaveToBin()
{
	int offsetTable = sizeof(int)*2 + 4 + 8 + 4; // 头 + 键值类型 + 数量
	int keyLen = 0, valLen = 0 , keyValLen = 0;

	zyMutexLock(m_mutex);

	keyLen = zyHashMap::GetTypeSize(m_keyType);
	keyValLen += keyLen;

	valLen = zyHashMap::GetTypeSize(m_valType);
	keyValLen += valLen;

	int offsetData = offsetTable + m_count * (keyLen + valLen);

	int buffLenth = offsetData;

	zyEntry* pe = NULL;

	if (m_keyType == SDT_TEXT || m_keyType == SDT_BIN || m_valType == SDT_TEXT || m_valType == SDT_BIN)
	{
		for (int i = 0; i < m_tableLenth; ++i)
		{
			pe = m_table[i];
			while (pe)
			{
				if (m_keyType == SDT_TEXT)
				{
					if (pe->key.pTextVal)
					{
						buffLenth += strlen(pe->key.pTextVal) + 1;
					}
				}
				else if(m_keyType == SDT_BIN)
				{
					if (pe->key.pBinVal)
					{
						buffLenth += *(LPINT)(pe->key.pBinVal + sizeof(int)) + sizeof(int) * 2;
					}
				}

				if (m_valType == SDT_TEXT)
				{
					if (pe->value.pTextVal)
					{
						buffLenth += strlen(pe->value.pTextVal) + 1;
					}
				}
				else if (m_valType == SDT_BIN)
				{
					if (pe->value.pBinVal)
					{
						buffLenth += *(LPINT)(pe->value.pBinVal + sizeof(int)) + sizeof(int) * 2;
					}
				}

				pe = pe->next;
			}
		}
	}

	LPBYTE pBuffer = (LPBYTE)MMalloc(buffLenth);
	memset(pBuffer, 0, buffLenth);
	*((LPINT)(pBuffer)) = 1;
	*((LPINT)(pBuffer + sizeof(int))) = buffLenth - sizeof(int) * 2;

	pBuffer[8] = 'h';
	pBuffer[9] = 'a';
	pBuffer[10] = 's';
	pBuffer[11] = 'h';

	*((LPINT)(pBuffer + 12)) = zyHashMap::GetEType(m_keyType);
	*((LPINT)(pBuffer + 16)) = zyHashMap::GetEType(m_valType);
	*((LPINT)(pBuffer + 20)) = m_count;

	int offset = 0;

	LPBYTE ptrTable = pBuffer + offsetTable;

	if (m_isOrder)
	{
		pe = m_orderHead;
		while (pe)
		{
			switch (m_keyType)
			{
			case SDT_BYTE:
				*ptrTable = pe->key.byteVal;
				ptrTable += sizeof(BYTE);
				break;
			case SDT_SHORT:
				*(short*)ptrTable = pe->key.shortVal;
				ptrTable += sizeof(short);
				break;
			case SDT_INT:
			case SDT_BOOL:
				*(int*)ptrTable = pe->key.intVal;
				ptrTable += sizeof(int);
				break;
			case SDT_INT64:
				*(__int64*)ptrTable = pe->key.int64Val;
				ptrTable += sizeof(__int64);
				break;
			case SDT_FLOAT:
				*(float*)ptrTable = pe->key.floatVal;
				ptrTable += sizeof(float);
				break;
			case SDT_DOUBLE:
			case SDT_DATE_TIME:
				*(double*)ptrTable = pe->key.doubleVal;
				ptrTable += sizeof(double);
				break;
			case SDT_SUB_PTR:
				*(void**)ptrTable = pe->key.value;
				ptrTable += sizeof(void*);
				break;
			case SDT_TEXT:
			{
				if (pe->key.pTextVal)
				{
					int len = strlen(pe->key.pTextVal);
					memcpy(pBuffer + offsetData + offset, pe->key.pTextVal, len);
					*(LPINT)ptrTable = offsetData + offset - sizeof(int) * 2;
					offset += len + 1;
				}
				else
				{
					*(LPINT)ptrTable = 0;
				}
				ptrTable += sizeof(char*);
				break;
			}
			case SDT_BIN:
			{
				if (pe->key.pBinVal)
				{
					int len = *(LPINT)(pe->key.pBinVal + sizeof(int)) + sizeof(int) * 2;
					memcpy(pBuffer + offsetData + offset, pe->key.pBinVal, len);
					*(LPINT)ptrTable = offsetData + offset - sizeof(int) * 2;
					offset += len;
				}
				else
				{
					*(LPINT)ptrTable = 0;
				}
				ptrTable += sizeof(LPBYTE);
				break;
			}

			default:
				break;
			}

			// 值
			switch (m_valType)
			{
			case SDT_BYTE:
				*ptrTable = pe->value.byteVal;
				ptrTable += sizeof(BYTE);
				break;
			case SDT_SHORT:
				*(short*)ptrTable = pe->value.shortVal;
				ptrTable += sizeof(short);
				break;
			case SDT_INT:
			case SDT_BOOL:
				*(int*)ptrTable = pe->value.intVal;
				ptrTable += sizeof(int);
				break;
			case SDT_INT64:
				*(__int64*)ptrTable = pe->value.int64Val;
				ptrTable += sizeof(__int64);
				break;
			case SDT_FLOAT:
				*(float*)ptrTable = pe->value.floatVal;
				ptrTable += sizeof(float);
				break;
			case SDT_DOUBLE:
			case SDT_DATE_TIME:
				*(double*)ptrTable = pe->value.doubleVal;
				ptrTable += sizeof(double);
				break;
			case SDT_SUB_PTR:
				*(void**)ptrTable = pe->value.value;
				ptrTable += sizeof(void*);
				break;
			case SDT_TEXT:
			{
				if (pe->value.pTextVal)
				{
					int len = strlen(pe->value.pTextVal);
					memcpy(pBuffer + offsetData + offset, pe->value.pTextVal, len);
					*(LPINT)ptrTable = offsetData + offset - sizeof(int) * 2;
					offset += len + 1;
				}
				else
				{
					*(LPINT)ptrTable = 0;
				}
				ptrTable += sizeof(char*);
				break;
			}
			case SDT_BIN:
			{
				if (pe->value.pBinVal)
				{
					int len = *(LPINT)(pe->value.pBinVal + sizeof(int)) + sizeof(int) * 2;
					memcpy(pBuffer + offsetData + offset, pe->value.pBinVal, len);
					*(LPINT)ptrTable = offsetData + offset - sizeof(int) * 2;
					offset += len;
				}
				else
				{
					*(LPINT)ptrTable = 0;
				}
				ptrTable += sizeof(LPBYTE);
				break;
			}

			default:
				break;
			}
			pe = pe->orderNext;
		}
	}
	else
	{
		for (int i = 0; i < m_tableLenth; ++i)
		{
			pe = m_table[i];
			while (pe)
			{
				switch (m_keyType)
				{
				case SDT_BYTE:
					*ptrTable = pe->key.byteVal;
					ptrTable += sizeof(BYTE);
					break;
				case SDT_SHORT:
					*(short*)ptrTable = pe->key.shortVal;
					ptrTable += sizeof(short);
					break;
				case SDT_INT:
				case SDT_BOOL:
					*(int*)ptrTable = pe->key.intVal;
					ptrTable += sizeof(int);
					break;
				case SDT_INT64:
					*(__int64*)ptrTable = pe->key.int64Val;
					ptrTable += sizeof(__int64);
					break;
				case SDT_FLOAT:
					*(float*)ptrTable = pe->key.floatVal;
					ptrTable += sizeof(float);
					break;
				case SDT_DOUBLE:
				case SDT_DATE_TIME:
					*(double*)ptrTable = pe->key.doubleVal;
					ptrTable += sizeof(double);
					break;
				case SDT_SUB_PTR:
					*(void**)ptrTable = pe->key.value;
					ptrTable += sizeof(void*);
					break;
				case SDT_TEXT:
				{
					if (pe->key.pTextVal)
					{
						int len = strlen(pe->key.pTextVal);
						memcpy(pBuffer + offsetData + offset, pe->key.pTextVal, len);
						*(LPINT)ptrTable = offsetData + offset - sizeof(int) * 2;
						offset += len + 1;
					}
					else
					{
						*(LPINT)ptrTable = 0;
					}
					ptrTable += sizeof(char*);
					break;
				}
				case SDT_BIN:
				{
					if (pe->key.pBinVal)
					{
						int len = *(LPINT)(pe->key.pBinVal + sizeof(int)) + sizeof(int) * 2;
						memcpy(pBuffer + offsetData + offset, pe->key.pBinVal, len);
						*(LPINT)ptrTable = offsetData + offset - sizeof(int) * 2;
						offset += len;
					}
					else
					{
						*(LPINT)ptrTable = 0;
					}
					ptrTable += sizeof(LPBYTE);
					break;
				}

				default:
					break;
				}

				// 值
				switch (m_valType)
				{
				case SDT_BYTE:
					*ptrTable = pe->value.byteVal;
					ptrTable += sizeof(BYTE);
					break;
				case SDT_SHORT:
					*(short*)ptrTable = pe->value.shortVal;
					ptrTable += sizeof(short);
					break;
				case SDT_INT:
				case SDT_BOOL:
					*(int*)ptrTable = pe->value.intVal;
					ptrTable += sizeof(int);
					break;
				case SDT_INT64:
					*(__int64*)ptrTable = pe->value.int64Val;
					ptrTable += sizeof(__int64);
					break;
				case SDT_FLOAT:
					*(float*)ptrTable = pe->value.floatVal;
					ptrTable += sizeof(float);
					break;
				case SDT_DOUBLE:
				case SDT_DATE_TIME:
					*(double*)ptrTable = pe->value.doubleVal;
					ptrTable += sizeof(double);
					break;
				case SDT_SUB_PTR:
					*(void**)ptrTable = pe->value.value;
					ptrTable += sizeof(void*);
					break;
				case SDT_TEXT:
				{
					if (pe->value.pTextVal)
					{
						int len = strlen(pe->value.pTextVal);
						memcpy(pBuffer + offsetData + offset, pe->value.pTextVal, len);
						*(LPINT)ptrTable = offsetData + offset - sizeof(int) * 2;
						offset += len + 1;
					}
					else
					{
						*(LPINT)ptrTable = 0;
					}
					ptrTable += sizeof(char*);
					break;
				}
				case SDT_BIN:
				{
					if (pe->value.pBinVal)
					{
						int len = *(LPINT)(pe->value.pBinVal + sizeof(int)) + sizeof(int) * 2;
						memcpy(pBuffer + offsetData + offset, pe->value.pBinVal, len);
						*(LPINT)ptrTable = offsetData + offset - sizeof(int) * 2;
						offset += len;
					}
					else
					{
						*(LPINT)ptrTable = 0;
					}
					ptrTable += sizeof(LPBYTE);
					break;
				}

				default:
					break;
				}
				pe = pe->next;
			}
		}
	}

	
	zyMutexUnLock(m_mutex);
	return pBuffer;
}

void zyHashMap::EnumItem(zyHashEnumCallback callback, void* parm)
{
	zyMutexLock(m_mutex);
	zyEntry* pe = NULL;
	if (m_isOrder)
	{
		pe = m_orderHead;
		while (pe)
		{
			if (callback(parm, &pe->key, &pe->value))
			{
				zyMutexUnLock(m_mutex);
				return;
			}
			pe = pe->orderNext;
		}
	}
	else
	{
		for (int i=0;i<m_tableLenth;++i)
		{
			pe = m_table[i];
			while (pe)
			{
				if (callback(parm, &pe->key, &pe->value))
				{
					zyMutexUnLock(m_mutex);
					return;
				}
				pe = pe->next;
			}
		}
	}
	zyMutexUnLock(m_mutex);
}

void zyHashMap::Destory()
{
	Release();
}

size_t zyHashMap::GetHashCode(PMDATA_INF key)
{
	switch (key->m_dtDataType)
	{
	case SDT_BYTE:
		return key->m_byte;
	case SDT_SHORT:
		return key->m_short;
	case SDT_INT:
		return key->m_int;
	case SDT_BOOL:
		return key->m_bool;
	case SDT_INT64:
		return BKDRHash((BYTE*)&key->m_int64, sizeof(__int64));
	case SDT_FLOAT:
		return BKDRHash((BYTE*)&key->m_float, sizeof(float));
	case SDT_DOUBLE:
	case SDT_DATE_TIME:
		return BKDRHash((BYTE*)&key->m_double, sizeof(double));
	case SDT_SUB_PTR:
		return (size_t)key->m_dwSubCodeAdr;
	case SDT_TEXT:
		return BKDRHash(key->m_pText);
	case SDT_BIN:
	{
		int len = *((int*)(key->m_pBin + sizeof(int)));
		return BKDRHash(key->m_pBin, len);
	}
	default:
		return 0;
	}
}

size_t zyHashMap::BKDRHash(const BYTE* ptr, size_t len)
{
	size_t hash = 0;
	for (size_t i =0; i<len;++i)
	{
		hash = hash * 31 + ptr[i];
	}
	return hash;
}

size_t zyHashMap::BKDRHash(const char* str)
{
	size_t hash = 0;
	while (*str)
	{
		hash = hash * 31 + (*str++);
	}
	return hash;
}

BOOL zyHashMap::E2Hash(DWORD valType, PMDATA_INF pArgInf, zyValue& val)
{
	switch (valType)
	{
	case SDT_BYTE:
		switch (pArgInf->m_dtDataType)
		{
		case SDT_FLOAT:
			val.byteVal = (BYTE)pArgInf->m_float;
			break;
		case SDT_DOUBLE:
		case SDT_DATE_TIME:
			val.byteVal = (BYTE)pArgInf->m_double;
			break;
		default:
			val.byteVal = pArgInf->m_byte;
			break;
		}
		return TRUE;
	case SDT_SHORT:
		switch (pArgInf->m_dtDataType)
		{
		case SDT_FLOAT:
			val.shortVal = (short)pArgInf->m_float;
			break;
		case SDT_DOUBLE:
		case SDT_DATE_TIME:
			val.shortVal = (short)pArgInf->m_double;
			break;
		default:
			val.shortVal = pArgInf->m_short;
			break;
		}
		return TRUE;
	case SDT_INT:
	case SDT_BOOL:
		switch (pArgInf->m_dtDataType)
		{
		case SDT_FLOAT:
			val.intVal = (int)pArgInf->m_float;
			break;
		case SDT_DOUBLE:
		case SDT_DATE_TIME:
			val.intVal = (int)pArgInf->m_double;
			break;
		default:
			val.intVal = pArgInf->m_int;
			break;
		}
		return TRUE;
	case SDT_INT64:
		switch (pArgInf->m_dtDataType)
		{
		case SDT_FLOAT:
			val.int64Val = (__int64)pArgInf->m_float;
			break;
		case SDT_DOUBLE:
		case SDT_DATE_TIME:
			val.int64Val = (__int64)pArgInf->m_double;
			break;
		default:
			val.int64Val = pArgInf->m_int64;
			break;
		}
		return TRUE;
	case SDT_FLOAT:
		switch (pArgInf->m_dtDataType)
		{
		case SDT_FLOAT:
			val.floatVal = pArgInf->m_float;
			break;
		case SDT_DOUBLE:
		case SDT_DATE_TIME:
			val.floatVal = (float)pArgInf->m_double;
			break;
		case SDT_INT64:
			val.floatVal = (float)pArgInf->m_int64;
			break;
		case SDT_BYTE:
			val.floatVal = (float)pArgInf->m_byte;
			break;
		case SDT_SHORT:
			val.floatVal = (float)pArgInf->m_short;
			break;
		default:
			val.floatVal = (float)pArgInf->m_int;
			break;
		}
		return TRUE;
	case SDT_DOUBLE:
	case SDT_DATE_TIME:
		switch (pArgInf->m_dtDataType)
		{
		case SDT_FLOAT:
			val.doubleVal = (double)pArgInf->m_float;
			break;
		case SDT_DOUBLE:
		case SDT_DATE_TIME:
			val.doubleVal = pArgInf->m_double;
			break;
		case SDT_INT64:
			val.doubleVal = (double)pArgInf->m_int64;
			break;
		case SDT_BYTE:
			val.doubleVal = (double)pArgInf->m_byte;
			break;
		case SDT_SHORT:
			val.doubleVal = (double)pArgInf->m_short;
			break;
		default:
			val.doubleVal = (double)pArgInf->m_int;
			break;
		}
		return TRUE;
	case SDT_SUB_PTR:
		val.value = (void*)pArgInf->m_dwSubCodeAdr;
		return TRUE;
	case SDT_TEXT:
		val.pTextVal = zyCloneText(pArgInf->m_pText);
		return TRUE;
	case SDT_BIN:
	{
		if (pArgInf->m_pBin)
		{
			int len = *((LPINT)(pArgInf->m_pBin + sizeof(int)));
			val.pBinVal = zyCloneBinData(pArgInf->m_pBin + sizeof(int) * 2, len);
		}
		else
		{
			val.pBinVal = NULL;
		}
	}
		
		return TRUE;
	default:
		val.int64Val = 0L;
		return FALSE;
	}
}

BOOL zyHashMap::Hash2E(DWORD valType, zyValue& val, PMDATA_INF pArgInf)
{
	pArgInf->m_dtDataType = valType;
	switch (valType)
	{
	case SDT_BYTE:
		pArgInf->m_byte = val.byteVal;
		return TRUE;
	case SDT_SHORT:
		pArgInf->m_short = val.shortVal;
		return TRUE;
	case SDT_INT:
		pArgInf->m_int = val.intVal;
		return TRUE;
	case SDT_BOOL:
		pArgInf->m_bool = val.intVal;
		return TRUE;
	case SDT_INT64:
		pArgInf->m_int64 = val.int64Val;
		return TRUE;
	case SDT_FLOAT:
		pArgInf->m_float = val.floatVal;
		return TRUE;
	case SDT_DOUBLE:
	case SDT_DATE_TIME:
		pArgInf->m_date = val.doubleVal;
		return TRUE;
	case SDT_SUB_PTR:
		pArgInf->m_dwSubCodeAdr = (DWORD)val.value;
		return TRUE;
	case SDT_TEXT:
		pArgInf->m_pText = CloneTextData(val.pTextVal);
		return TRUE;
	case SDT_BIN:
	{
		if (val.pBinVal)
		{
			int len = *((LPINT)(val.pBinVal + sizeof(int)));
			pArgInf->m_pBin = CloneBinData(val.pBinVal + sizeof(int) * 2, len);
		}
		else
		{
			pArgInf->m_pBin = NULL;
		}
	}
		return TRUE;
	default:
		return FALSE;
	}
}

BOOL zyHashMap::Hash2Hash(DWORD valType, zyValue& valSrc, zyValue& val)
{
	switch (valType)
	{
	case SDT_BYTE:
		val.byteVal = valSrc.byteVal;
		return TRUE;
	case SDT_SHORT:
		val.shortVal = valSrc.shortVal;
		return TRUE;
	case SDT_INT:
		val.intVal = valSrc.intVal;
		return TRUE;
	case SDT_BOOL:
		val.intVal = valSrc.intVal;
		return TRUE;
	case SDT_INT64:
		val.int64Val = valSrc.int64Val;
		return TRUE;
	case SDT_FLOAT:
		val.floatVal = valSrc.floatVal;
		return TRUE;
	case SDT_DOUBLE:
	case SDT_DATE_TIME:
		val.doubleVal = valSrc.doubleVal;
		return TRUE;
	case SDT_SUB_PTR:
		val.value = valSrc.value;
		return TRUE;
	case SDT_TEXT:
		val.pTextVal = zyCloneText(valSrc.pTextVal);
		return TRUE;
	case SDT_BIN:
	{
		if (valSrc.pBinVal)
		{
			int len = *((LPINT)(valSrc.pBinVal + sizeof(int)));
			val.pBinVal = zyCloneBinData(valSrc.pBinVal + sizeof(int) * 2, len);
		}
		else
		{
			val.pBinVal = NULL;
		}
	}
	return TRUE;
	default:
		return FALSE;
	}
}

LPBYTE zyHashMap::SetData(DWORD valType, LPBYTE ptrData, const zyValue& val)
{
	switch (valType)
	{
	case SDT_BYTE:
		*(ptrData) = val.byteVal;
		return ptrData + sizeof(BYTE);
	case SDT_SHORT:
		*((short*)ptrData) = val.shortVal;
		return ptrData + sizeof(short);
	case SDT_INT:
	case SDT_BOOL:
		*((int*)ptrData) = val.intVal;
		return ptrData + sizeof(int);
	case SDT_INT64:
		*((__int64*)ptrData) = val.int64Val;
		return ptrData + sizeof(__int64);
	case SDT_FLOAT:
		*((float*)ptrData) = val.floatVal;
		return ptrData + sizeof(float);
	case SDT_DOUBLE:
	case SDT_DATE_TIME:
		*((double*)ptrData) = val.doubleVal;
		return ptrData + sizeof(double);
	case SDT_SUB_PTR:
		*((DWORD*)ptrData) = (DWORD)val.value;
		return ptrData + sizeof(DWORD);
	case SDT_TEXT:
		*((char**)ptrData) = CloneTextData(val.pTextVal);
		return ptrData + sizeof(char*);
	case SDT_BIN:
	{
		if (val.pBinVal)
		{
			int len = *((int*)(val.pBinVal + sizeof(int)));
			*((LPBYTE*)ptrData) = CloneBinData(val.pBinVal + sizeof(int) * 2, len);
		}
		else
		{
			*((LPBYTE*)ptrData) = NULL;
		}
		return ptrData + sizeof(LPBYTE);
	}
	default:
		break;
	}
	return NULL;
}

int zyHashMap::GetEType(DWORD valType)
{
	switch (valType)
	{
	case SDT_BYTE:
		return 1;
	case SDT_SHORT:
		return 2;
	case SDT_INT:
		return 3;
	case SDT_INT64:
		return 4;
	case SDT_FLOAT:
		return 5;
	case SDT_DOUBLE:
		return 6;
	case SDT_BOOL:
		return 7;
	case SDT_DATE_TIME:
		return 8;
	case SDT_SUB_PTR:
		return 9;
	case SDT_TEXT:
		return 10;
	case SDT_BIN:
		return 11;
	default:
		return 0;
	}
}

DWORD zyHashMap::GetLibType(int eType)
{
	switch (eType)
	{
	case 1:
		return SDT_BYTE;
	case 2:
		return SDT_SHORT;
	case 3:
		return SDT_INT;
	case 4:
		return SDT_INT64;
	case 5:
		return SDT_FLOAT;
	case 6:
		return SDT_DOUBLE;
	case 7:
		return SDT_BOOL;
	case 8:
		return SDT_DATE_TIME;
	case 9:
		return SDT_SUB_PTR;
	case 10:
		return SDT_TEXT;
	case 11:
		return SDT_BIN;
	default:
		return _SDT_NULL;
	}
}

int zyHashMap::GetTypeSize(DWORD libType)
{
	int len = 0;
	switch (libType)
	{
	case SDT_BYTE:
		len = sizeof(BYTE);
		break;
	case SDT_SHORT:
		len = sizeof(short);
		break;
	case SDT_DATE_TIME:
	case SDT_DOUBLE:
	case SDT_INT64:
		len = sizeof(double);
	default:
		len = sizeof(int);
		break;
	}
	return len;
}

BOOL zyHashMap::Equal(DWORD keyType, PMDATA_INF pArgInf, zyValue& key)
{
	const double EPSINON = 0.00001;
	switch (keyType)
	{
	case SDT_BYTE:
		return pArgInf->m_byte == key.byteVal;
	case SDT_SHORT:
		return pArgInf->m_short == key.shortVal;
	case SDT_INT:
		return pArgInf->m_int == key.intVal;
	case SDT_BOOL:
		return pArgInf->m_bool == key.intVal;
	case SDT_INT64:
		return pArgInf->m_int64 == key.int64Val;
	case SDT_FLOAT:
	{
		float x = pArgInf->m_float - key.floatVal;
		if ((x >= -EPSINON) && (x <= EPSINON))
		{
			return TRUE;
		}
		return FALSE;
	}
	case SDT_DOUBLE:
	{
		double x = pArgInf->m_double - key.doubleVal;
		if ((x >= -EPSINON) && (x <= EPSINON))
		{
			return TRUE;
		}
		return FALSE;
	}
	case SDT_DATE_TIME:
	{
		double x = pArgInf->m_date - key.doubleVal;
		if ((x >= -EPSINON) && (x <= EPSINON))
		{
			return TRUE;
		}
		return FALSE;
	}
	case SDT_SUB_PTR:
		return pArgInf->m_dwSubCodeAdr == (DWORD)key.value;
	case SDT_TEXT:
		return strcmp(pArgInf->m_pText, key.pTextVal) == 0;
	case SDT_BIN:
	{	if (pArgInf->m_pBin == key.pBinVal)
		{
			return TRUE;
		}
		if (pArgInf->m_pBin == NULL || key.pBinVal == NULL)
		{
			return FALSE;
		}
		int len = *((PINT)(pArgInf->m_pBin + sizeof(int)));
		int len2 = *((PINT)(key.pBinVal + sizeof(int)));
		if (len != len2)
		{
			return FALSE;
		}
		LPBYTE ptr1 = pArgInf->m_pBin + sizeof(int) * 2;
		LPBYTE ptr2 = key.pBinVal + sizeof(int) * 2;
		for (int i=0; i<len;++i)
		{
			if (ptr1[i] != ptr2[i])
			{
				return FALSE;
			}
		}
		return TRUE;
	}
	default:
		return FALSE;
	}
}

bool zyHashMap::ReHash()
{
	size_t oldLenth = m_tableLenth;
	zyEntry** oldTable = m_table;
	size_t newLenth = oldLenth * 2 + 1;
	zyEntry ** newTable = (zyEntry**)malloc(newLenth * sizeof(zyEntry*));
	if (!newTable)
	{
		return false;
	}
	memset(newTable, 0, newLenth * sizeof(zyEntry*));
	m_tableLenth = newLenth;
	m_table = newTable;
	m_threshold = (int)(m_tableLenth * m_loadFactor);
	for (size_t i = 0; i < oldLenth; i++)
	{
		zyEntry* pe = oldTable[i];
		while (pe)
		{
			zyEntry* oldNext = pe->next;
			size_t index = (pe->hashCode & 0x7FFFFFFF) % m_tableLenth;
			pe->next = m_table[index];
			m_table[index] = pe;
			pe = oldNext;
		}
	}

	free(oldTable);
	return true;
}
