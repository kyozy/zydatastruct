#pragma once

#include <windows.h>
#include <stdlib.h>
#include "string.h"
#include "elib_sdk/lib2.h"
#include "elib_sdk/fnshare.h"

char* zyCloneText(const char* pStr);
LPBYTE zyCloneBinData(LPBYTE pData, INT nDataSize);

class CFreqMem
{
private:
	unsigned char* m_pdata;
	size_t m_datalen, m_bufferlen;
public:
	CFreqMem()
	{
		m_pdata = NULL; m_datalen = m_bufferlen = 0;
	}
	void* GetPtr()
	{
		return (m_datalen == 0 ? NULL : m_pdata);
	}
	void AddDWord(DWORD dw)
	{
		AppendData(&dw, sizeof(dw));
	}
	void AppendData(void* pdata, size_t size)
	{
		if (m_bufferlen - m_datalen < size)
		{
			if (m_pdata == NULL)
			{
				m_bufferlen = 128;
				m_pdata = (unsigned char*)MMalloc(m_bufferlen);
				//assert(m_datalen == 0);
			}
			size_t oldLen = m_bufferlen;
			while (m_bufferlen - m_datalen < size)
			{
				m_bufferlen *= 2;
			};

			if (oldLen < m_bufferlen)
			{
				unsigned char* newData = (unsigned char*)MMalloc(m_bufferlen);
				memcpy(newData, m_pdata, oldLen);
				MFree(m_pdata);
				m_pdata = newData;
			}
			
		}
		memcpy(m_pdata + m_datalen, pdata, size);
		m_datalen += size;
	}
};