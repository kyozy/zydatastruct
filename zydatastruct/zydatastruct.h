#pragma once

#include <Windows.h>
#include "zyHashMap.h"

#ifndef __E_STATIC_LIB

#define LIB_GUID_STR "A217DD6ACD59422EABE0CB984BECD30F" /*GUID串: {0B334DD2-57F4-466A-8D53-09256414C0B3} */
#define LIB_MajorVersion 1 /*库主版本号*/
#define LIB_MinorVersion 0 /*库次版本号*/
#define LIB_BuildNumber 0 /*构建版本号*/
#define LIB_SysMajorVer 3 /*系统主版本号*/
#define LIB_SysMinorVer 0 /*系统次版本号*/
#define LIB_KrnlLibMajorVer 3 /*核心库主版本号*/
#define LIB_KrnlLibMinorVer 0 /*核心库次版本号*/
#define LIB_NAME_STR "ZY数据结构支持库" /*支持库名*/
#define LIB_DESCRIPTION_STR "" /*功能描述*/
#define LIB_Author "kyozy" /*作者名称*/
#define LIB_ZipCode "644000" /*邮政编码*/
#define LIB_Address "" /*通信地址*/
#define LIB_Phone	"" /*电话号码*/
#define LIB_Fax		"75602718" /*QQ号码*/
#define LIB_Email	 "" /*电子邮箱*/
#define LIB_HomePage "" /*主页地址*/
#define LIB_Other	"" /*其它信息*/
#define LIB_TYPE_COUNT 1 /*命令分类数量*/
#define LIB_TYPE_STR	"0000基本命令\0" \
						"\0" /*命令分类*/
#endif

struct zyHashMapStruct
{
	zyHashMap* hHashMap;
};

#define SETUP_HASHMAP(x)	zyHashMap* hHashMap = ((zyHashMapStruct*)x[0].m_pCompoundData)->hHashMap

#define DTP_HASHMAP		MAKELONG(1,0)
//#define DTP_STRUCT2		MAKELONG(2,0)

