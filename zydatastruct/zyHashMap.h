#pragma once
#include <windows.h>
#include <stdlib.h>
#include "elib_sdk/lib2.h"

struct IZYKnown
{
	virtual unsigned long  __stdcall QueryInterface(const char* iid, IZYKnown **ppv) = 0;
	///引用接口，引用计数加一
	virtual unsigned long  __stdcall AddRef() = 0;
	///释放接口，引用计数减一，计数为0时释放接口的实现对象。
	virtual unsigned long  __stdcall Release() = 0;
};

struct zyValue
{
	union 
	{
		unsigned char byteVal;
		short shortVal;
		int	intVal;
		__int64 int64Val;
		float floatVal;
		double doubleVal;
		char* pTextVal;
		BYTE* pBinVal;
		void* value;
	};
};

struct zyEntry
{
	size_t hashCode;
	zyValue key;
	zyValue value;
	zyEntry* next;
	zyEntry* orderPrev;
	zyEntry* orderNext;
};

typedef BOOL(__stdcall *zyHashEnumCallback)(void* parm, zyValue* key, zyValue* value);

class zyHashMap:public IZYKnown
{
private:
	long m_refCount;
	zyEntry** m_table;	//数组
	int m_tableLenth;	//数组长度
	int m_count;		//数量
	int m_threshold; //扩容临界值
	float m_loadFactor; //载入因子
	DWORD m_keyType; // SDT_
	DWORD m_valType; // SDT_
	zyEntry* m_orderHead; //有序时的链表头
	BOOL m_isOrder; //是否有序
	HANDLE m_mutex; //互斥锁

public:
	zyHashMap(DWORD keyType, DWORD valType, BOOL isOrder = false, BOOL threadSafe = false, int initialCapacity = 11, float loadFactor = 0.75);

	static zyHashMap* FromBytes (LPBYTE bytes, int length, BOOL isOrder = false, BOOL threadSafe = false, int initialCapacity = 11, float loadFactor = 0.75);

	virtual ~zyHashMap();

	virtual unsigned long __stdcall QueryInterface(const char* iid, IZYKnown **ppv);


	virtual unsigned long __stdcall AddRef();


	virtual unsigned long __stdcall Release();

	int Put(PMDATA_INF pArgKey, PMDATA_INF pArgVal);

	void Get(PMDATA_INF pRet, PMDATA_INF pArgKey);

	BOOL Contains(PMDATA_INF pArgVal);

	BOOL ContainsKey(PMDATA_INF pArgKey);

	BOOL Remove(PMDATA_INF pArgKey);

	void Clear();

	int GetCount() const;

	int GetAllKeys(PMDATA_INF pArgKeys);

	int GetAllValues(PMDATA_INF pArgValues);

	int GetKeysFromValue(PMDATA_INF pArgValue, PMDATA_INF pArgKeys);

	zyHashMap* Clone ();

	LPBYTE SaveToBin();

	void EnumItem(zyHashEnumCallback callback, void* parm);

	void Destory();

private:
	static inline size_t GetHashCode(PMDATA_INF key);
	static inline size_t BKDRHash(const BYTE* ptr, size_t len);
	static inline size_t BKDRHash(const char* str);
	static inline BOOL E2Hash(DWORD valType, PMDATA_INF pArgInf, zyValue& val);
	static inline BOOL Hash2E(DWORD valType, zyValue& val, PMDATA_INF pArgInf);
	static inline BOOL Hash2Hash(DWORD valType, zyValue& valSrc, zyValue& val);
	static inline BOOL Equal(DWORD keyType, PMDATA_INF pArgInf, zyValue& key);
	static inline LPBYTE SetData(DWORD valType, LPBYTE ptrData,const zyValue& val);
	static inline int GetEType(DWORD valType);
	static inline DWORD GetLibType(int eType);
	static inline int GetTypeSize(DWORD libType);

	bool ReHash();
};

