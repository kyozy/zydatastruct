#include "helper.h"

char* zyCloneText(const char* pStr)
{
	if (pStr == NULL || *pStr == '\0')
		return NULL;

	INT nTextLen = strlen(pStr);
	char* pd = (char*)malloc(nTextLen + 1);
	memcpy(pd, pStr, nTextLen);
	pd[nTextLen] = '\0';
	return pd;
}

LPBYTE zyCloneBinData(LPBYTE pData, INT nDataSize)
{
	if (nDataSize == 0)
		return NULL;

	LPBYTE pd = (LPBYTE)malloc(sizeof(INT) * 2 + nDataSize);
	*(LPINT)pd = 1;
	*(LPINT)(pd + sizeof(INT)) = nDataSize;
	memcpy(pd + sizeof(INT) * 2, pData, nDataSize);
	return pd;
}