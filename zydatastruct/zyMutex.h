#pragma once
#include <windows.h>

HANDLE zyMutexInit();
void zyMutexDestory(HANDLE hMutex);
void zyMutexLock(HANDLE hMutex);
BOOL zyMutexUnLock(HANDLE hMutex);

